import json
import pathlib

EMAIL = "email"
DELIVERED = "DELIVERED"
PAID = "PAID"
OPEN = "OPEN"
ITEMS = "items"
STATUS = "status"
PRICE = "price"
QUANTITY = "quantity"
NAME = "name"
TOTAL = "total"


def get_file_data(file_name):
    directory_path = pathlib.Path("./")
    file_path = directory_path / file_name

    try:
        with open(file_path, "r") as json_file:
            file_data = json.load(json_file)
            return file_data
    except IOError:
        raise FileNotFoundError(f"Failed opening file: {file_name}.")


def get_customer_baskets(email, all_shopping_baskets):
    customer_shopping_baskets = [
        customer_basket
        for customer_basket in all_shopping_baskets
        if customer_basket[EMAIL] == email and len(customer_basket[ITEMS]) > 0
    ]

    return customer_shopping_baskets


def get_all_customers(all_shopping_baskets):
    all_customer_emails = list(
        set(customer_basket[EMAIL] for customer_basket in all_shopping_baskets)
    )
    all_customer_emails.sort()
    return all_customer_emails


def get_required_stock(all_shopping_baskets):
    items_to_deliver = []
    for customer_basket in all_shopping_baskets:
        if customer_basket[STATUS] == PAID:
            items_to_deliver += customer_basket[ITEMS]

    non_duplicate_items = list(set(item[NAME] for item in items_to_deliver))
    non_duplicate_items.sort()

    required_items = []
    for common_item in non_duplicate_items:
        quantity = 0
        for item in items_to_deliver:
            if item[NAME] == common_item:
                quantity += item[QUANTITY]
        required_items.append({NAME: common_item, QUANTITY: quantity})

    return required_items


def get_total_spent(email, all_shopping_baskets):
    customer_baskets = get_customer_baskets(email, all_shopping_baskets)

    delivered_or_paid_items = []
    for customer_basket_details in customer_baskets:
        if customer_basket_details[STATUS] in [DELIVERED, PAID]:
            delivered_or_paid_items += customer_basket_details[ITEMS]

    total_spent = sum(
        item_delivered_or_paid[PRICE] * item_delivered_or_paid[QUANTITY]
        for item_delivered_or_paid in delivered_or_paid_items
    )
    return total_spent


def get_top_customers(all_shopping_baskets):
    all_emails = get_all_customers(all_shopping_baskets)

    paid_customers = [
        {EMAIL: email, TOTAL: get_total_spent(email, all_shopping_baskets)}
        for email in all_emails
    ]

    top_paid_customers = sorted(
        paid_customers,
        key=lambda email_or_amount_per_customer: email_or_amount_per_customer[TOTAL],
        reverse=True,
    )

    return top_paid_customers


def get_customers_with_open_baskets(all_shopping_baskets):
    customers_with_open_baskets = list(
        set(
            customer_basket[EMAIL]
            for customer_basket in all_shopping_baskets
            if customer_basket[STATUS] == OPEN
        )
    )
    customers_with_open_baskets.sort()
    return customers_with_open_baskets


if __name__ == "__main__":
    file_name = "data.json"

    file_data = get_file_data(file_name)

    print(get_customer_baskets("mo@umuzi.org", file_data))
    print()
    print(get_all_customers(file_data))
    print()
    print(get_required_stock(file_data))
    print()
    print(get_total_spent("ryan@umuzi.org", file_data))
    print()
    print(get_total_spent("mo@umuzi.org", file_data))
    print()
    print(get_top_customers(file_data))
    print()
    print(get_customers_with_open_baskets(file_data))
